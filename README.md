# TP-request-expansion-ranking



## Description du projet

Ce projet est réalisé dans le cadre d'un TP d'Indexation Web à l'ENSAI. L'objectif est de construire un système permettant de répondre à la requête d'un utilisateur grâce à un index de titres de pages web, préalablement fourni. L'implémentation est réalisée en Python.

## Exécution du code

```
git clone https://gitlab.com/maelle1234/tp-request-expansion-ranking.git
cd tp-request-expansion-ranking
python3 main.py
```

## Fonctionnement du code

Ce système est implémenté dans `search/my_search.py`, via la classe `mySearch`. Elle comprend plusieurs méthodes correspondant aux différentes étapes nécessaires pour intercepter puis répondre à la requête de l'utilisateur, combinées dans la méthode `search()` qui permet de saisir une requête et d'afficher ses résultats. La requête utilisateur est d'abord tokenisée de la même manière que l'index, on cherche ensuite tous les documents qui contient tout ou partie de la requête (paramètre utilisateur à choisir), enfin on ordonne les résultats obtenus en fonction de leur pertinence vis à vis de la requête (calculée grâce au nombre de tokens de la requête présents dans le titre, et de la correspondance avec le premier mot). On peut également sauvegarder les résultats obtenus et des metadata sur la requête effectuée grâce à `save_results()`, qui sauvegarde le fichier dans `output`.

Le code permettant de lancer une requête et de la sauvegarder est contenu dans le fichier `main.py`. 

Des test unitaires ont été réalisés et sont présents dans le dossier `tests`. Pour les lancer, on peut exécuter la commande suivante:

```
python3 -m unittest
```

### Contributeurs

Maëlle Cosson