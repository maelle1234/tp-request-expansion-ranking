import json

class mySearch:

    """ Create a system to answer a user query, including a simple ranking mechanism for the results 

    Parameter
    ---------
    index_path : str
        path of the index
    
    Attributes
    ----------
    index : dict
        index used to answer user requests
    r : str
        user request
    r_tokens : list
        tokenized user request
    r_type : str
        'ALL' if the user wants to match all tokens in their request, 
        'ANY' if the user wants to match any of the tokens
    document_matches : list
        document ids that have matched user request
    ranked_results : dict
        result of the query sorted by documents' scores
    final_results : dict
        formatted result including a metadata field and the different results (ranked)
        in a more understandable format

    """

    def __init__(self, index_path):

        with open(index_path) as f:
            self.index = json.load(f)

        self.r = ''
        self.r_tokens = []
        self.r_type = 'NA'
        self.document_matches = []
        self.ranked_results = []
        self.final_results = {}


    def user_request(self):
        """ Ask and tokenize a user request """
        r = input("Please enter your request: ")
        self.r = r
        r_token = r.lower().split()
        self.r_tokens = r_token
        r_type = input("Are you looking to match all the words in this request or any of these words? \nPlease enter ALL or ANY: ").upper()
        while r_type not in ['ALL', 'ANY']:
            print("Error! Answer must be ANY or ALL.")
            r_type = input("Are you looking to match all the words in this request or any of these words? \nPlease enter ALL or ANY: ").upper()
        self.r_type = r_type


    def get_documents_with_token(self, token):
        """ Find the documents containing a given token """
        if token in self.index.keys():
            res = set(self.index[token].keys())
        else:
            res = set()
        return res

    
    def find_document_matches(self):
        """ Find the documents matching each or any token in a request """
        if len(self.r_tokens) > 0:
            # on considère tous les documents qui contiennent le premier token de la requête
            document_matches = set(self.get_documents_with_token(self.r_tokens[0]))
            if len(self.r_tokens) > 1:
                for t in self.r_tokens[1:]:
                    t_found_in = set(self.get_documents_with_token(t))
                    if self.r_type == 'ALL':
                        document_matches = document_matches.intersection(t_found_in)
                    elif self.r_type == 'ANY':
                        document_matches = document_matches.union(t_found_in)
            self.document_matches = document_matches
        else:
            print('Error! Your request is empty, please try again.')

    
    def count_tokens_in_doc(self, doc_id):
        """ Count the number of tokens from the request in a given document """
        doc_id = str(doc_id)
        count = 0
        for token in self.r_tokens:
            if token in self.index.keys() and doc_id in self.index[token]:
                count += 1
        return count


    def is_first_word(self, doc_id):
        """ Return true if the first token of the request is also the first token in the document's title, false otherwise or if 
        the token is not in that document """
        doc_id = str(doc_id)
        res = False
        if len(self.r_tokens) != 0:
            if self.r_tokens[0] in self.index.keys():
                if doc_id in self.index[self.r_tokens[0]] and 0 in self.index[self.r_tokens[0]][doc_id]['positions']:
                    res = True
        return res


    def rank_results(self):
        """ Compute a score for each document depending on token count and first word and sort results
        according to that score """
        doc_pre_ranked = {}
        for doc_id in self.document_matches:
            doc_score = self.count_tokens_in_doc(doc_id) + 5*self.is_first_word(doc_id)
            doc_pre_ranked[doc_id] = doc_score
        doc_ranked = sorted(doc_pre_ranked.items(), key=lambda x:x[1], reverse=True)
        self.ranked_results = doc_ranked


    def format_results(self):
        """ Add metadata information and other result information to make it more readable """

        with open('documents.json') as f:
            documents_info = json.load(f)

        #metadata
        self.final_results['metadata'] = {'nb_documents_index':len(documents_info), 
                                            'request':self.r,
                                            'nb_results':len(self.document_matches)}
        self.final_results['results'] = []

        #results
        for doc in self.ranked_results:
            doc_id = doc[0]
            doc_title = [d['title'] for d in documents_info if d['id'] == int(doc_id)][0]
            #print(doc_title)
            doc_url = [d['url'] for d in documents_info if d['id'] == int(doc_id)][0]
            self.final_results['results'].append({'title':doc_title, 'url':doc_url})


    def save_result(self):
        """ Save formated result as a .json file """
        with open('output/results.json', 'w', encoding="utf-8") as f:
            json.dump(self.final_results, f, ensure_ascii=False, indent=4)


    def search(self):
        """ Combine steps until saving and display results in a readable way """
        self.user_request()
        self.find_document_matches()
        self.rank_results()
        self.format_results()
        print('------------------------------------------')
        for res in self.final_results['results']:
            print(res['title'])
            print(res['url'])
            print('')


#print(rr.index)
#print(rr.get_documents_with_token('wikipédia'))
#rr.user_request()
#rr.find_document_matches()
#print(rr.document_matches)
#doc_id = input()
#print(rr.count_tokens_in_doc(doc_id))
#print(rr.is_first_word(doc_id))
#rr.rank_results()
#print(rr.format_results())
