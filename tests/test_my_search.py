import unittest
from search.my_search import mySearch

class testMySearch(unittest.TestCase):

    def test_get_documents_with_token(self):
        s = mySearch('index.json')
        token = 'karine'
        res = s.get_documents_with_token(token)
        self.assertIn(str(0), res)
        self.assertEqual(len(res), 1)

    def test_get_documents_with_token_unavailable(self):
        s = mySearch('index.json')
        res = s.get_documents_with_token('jszanfck')
        self.assertEqual(len(res), 0)

    def test_count_tokens_in_doc(self):
        s = mySearch('index.json')
        s.r_tokens = ['karine', 'wikipédia', 'tomate']
        res = s.count_tokens_in_doc(0)
        self.assertEqual(res, 2)

    def test_count_tokens_in_doc_none(self):
        s = mySearch('index.json')
        s.r_tokens = ['kjf', 'aaaaaa']
        res = s.count_tokens_in_doc(7)
        self.assertEqual(res, 0)
        
    def test_is_first_word(self):
        s = mySearch('index.json')
        s.r_tokens = ['karine', 'wikipédia']
        res = s.is_first_word(0)
        self.assertTrue(res)

    def test_is_first_word_isnt(self):
        s = mySearch('index.json')
        s.r_tokens = ['wikipédia', 'karine']
        res = s.is_first_word(0)
        self.assertFalse(res)

    def test_is_first_word_not_token(self):
        s = mySearch('index.json')
        s.r_tokens = ['dkjjj']
        res = s.is_first_word(345)
        self.assertFalse(res)

    def test_is_first_word_empty(self):
        s = mySearch('index.json')
        s.r_tokens = []
        res = s.is_first_word(77)
        self.assertFalse(res)